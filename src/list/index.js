import React, { useState } from 'react';
import Accordion from 'react-bootstrap/Accordion';
import Button from 'react-bootstrap/Button';
import Badge from 'react-bootstrap/Badge'
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { KEYS } from '../store';
import Question from '../network/model/question';

function List({ questions, fromdate, ...props }) {

	const [activeKey, setActiveKey] = useState(undefined);

	document.querySelector('body').addEventListener('click', () => setActiveKey('-1'));

	const addScore = (event, id) => {
		const patch = questions.map((item) => {
			const newScore = item.score + 1;
			const newQuestion = item.question_id === id ? Question.constructFromObject({ score: newScore }, item) : item;

			return newQuestion;
		});

		props.dispatch({
			type: KEYS.QUESTIONS_PATCH, patch
		});

		event.target.focus();
	};

	const reduceScore = (event, id) => {
		const patch = questions.map((item) => {
			const newScore = item.score - 1;
			const newQuestion = item.question_id === id ? Question.constructFromObject({ score: newScore }, item) : item;

			return newQuestion;
		});

		props.dispatch({
			type: KEYS.QUESTIONS_PATCH, patch
		});

		event.target.focus();
	};

	return <>
		<Accordion defaultActiveKey="-1" activeKey={activeKey}>
			{questions.sort((a, b) => b.score - a.score).map(({
				is_answered,
				last_activity_date,
				owner,
				question_id,
				score,
				title,
				view_count,
			}, i) => (
					<Card key={i}>
						<Card.Header className={classNames('d-flex', 'justify-content-between', { 'bg-success': is_answered })}>
							<Accordion.Toggle
								className="text-trunc"
								as={Button}
								variant={is_answered ? 'success' : 'light'}
								eventKey={String(question_id)}
								onClick={() => setActiveKey(String(question_id))}
							>
								{title}
							</Accordion.Toggle>
							<div className="d-flex align-items-center">

								<Badge className="mr-2" variant="secondary">{score}</Badge>
								<div className="d-flex flex-column">
									<Button
										size="sm"
										variant={is_answered ? 'success' : 'light'}
										className="font-weight-bold my-1"
										onClick={(event) => addScore(event, question_id)}>&uArr;</Button>
									<Button
										size="sm"
										variant={is_answered ? 'success' : 'light'}
										className="font-weight-bold my-1"
										onClick={(event) => reduceScore(event, question_id)}>&dArr;</Button>
								</div>

							</div>
						</Card.Header>
						<Accordion.Collapse eventKey={String(question_id)}>
							<Card.Body>
								<ListGroup variant="flush">
									<ListGroup.Item>Автор: {owner.display_name}</ListGroup.Item>
									<ListGroup.Item>Рейтинг: {owner.reputation}</ListGroup.Item>
									<ListGroup.Item>Количество просмотров: {view_count}</ListGroup.Item>
									<ListGroup.Item>Дата последней активности: {String(new Date(Number(last_activity_date + '000')))}</ListGroup.Item>
								</ListGroup>
							</Card.Body>
						</Accordion.Collapse>
					</Card>
				))}
		</Accordion>
	</>
};

export default connect(({ questions, fromdate }) => ({ questions, fromdate }))(List);