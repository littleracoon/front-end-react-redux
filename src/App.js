import React from 'react';
import { Provider } from 'react-redux';
import initStore from './store';
import Page from './page';

import './App.scss';

function App() {

  return (
    <Provider store={initStore()}>
      <Page />
    </Provider >
  );
}

export default App;
