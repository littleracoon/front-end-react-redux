import * as queryString from 'query-string';

function formatDateToSend(date = '') {
    const dateNow = new Date(date).getTime();

    return Number(String(dateNow).slice(0, 10));
}

export default function getList(date) {
    const path = queryString.stringifyUrl({
        url: 'https://api.stackexchange.com/2.2/questions',
        query: {
            // fromdate: 1514764800,
            fromdate: formatDateToSend(date),
            order: 'desc',
            sort: 'votes',
            tagged: 'react;redux',
            // tagged: 'react-redux',
            site: 'stackoverflow',
            pagesize: 100
        },
    });

    return fetch(path)
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            else {
                throw new Error("Что-то пошло не так. Код ошибки: " + response.status);
            }
        })
};
