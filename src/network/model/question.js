class Question {

    constructor(creation_date, is_answered, last_activity_date, owner, question_id, score, title, view_count,) {
        Question.initialize(this, creation_date, is_answered, last_activity_date, owner, question_id, score, title, view_count,);
    }

    static initialize(obj, creation_date, is_answered, last_activity_date, owner, question_id, score, title, view_count,) {
        obj['creation_date'] = creation_date;
        obj['is_answered'] = is_answered;
        obj['last_activity_date'] = last_activity_date;
        obj['owner'] = owner;
        obj['question_id'] = question_id;
        obj['score'] = score;
        obj['title'] = title;
        obj['view_count'] = view_count;
    }

    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new Question();

            if (data.hasOwnProperty('creation_date')) {
                obj['creation_date'] = String(data['creation_date']);
            }
            if (data.hasOwnProperty('is_answered')) {
                obj['is_answered'] = Boolean(data['is_answered']);
            }
            if (data.hasOwnProperty('last_activity_date')) {
                obj['last_activity_date'] = String(data['last_activity_date']);
            }
            if (data.hasOwnProperty('owner')) {
                obj['owner'] = data['owner'];
            }
            if (data.hasOwnProperty('question_id')) {
                obj['question_id'] = Number(data['question_id']);
            }
            if (data.hasOwnProperty('score')) {
                obj['score'] = Number(data['score']);
            }
            if (data.hasOwnProperty('title')) {
                obj['title'] = String(data['title']);
            }
            if (data.hasOwnProperty('view_count')) {
                obj['view_count'] = Number(data['view_count']);
            }
        }
        return obj;
    }
}

export default Question;