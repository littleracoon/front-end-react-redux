import React, { useState, useEffect } from 'react';
import getList from '../network/get';
import { KEYS } from '../store';
import Question from '../network/model/question';
import { connect } from 'react-redux';
import { Container } from 'react-bootstrap';
import List from '../list';
import DateForm from '../form';

function Page({ questions, fromdate, ...props }) {

	const [error, setError] = useState('');

	useEffect(() => {

		getList(fromdate)
			.then(response => {
				const items = response.items.filter(item => item.title.indexOf('react-redux') > -1).slice(0, 5);

				if (!items || !items.length) {
					return setError('Вопросы не найдены');
				};

				const patch = items.map((item) => (Question.constructFromObject(item)))

				props.dispatch({
					type: KEYS.QUESTIONS_PATCH, patch
				});
			})
			.catch(error => setError(error.message))

	}, [fromdate]);

	return <>
		<div className="bg-light p-4 mb-5">
			<Container>
				<h1 className="h3 mb-3">5 самых популярных вопросов на stackoverFlow, содержащих "react-redux".</h1>
				<div className="d-flex align-items-center">
					<h2 className="h5 my-2 mr-3">Фильтровать по дате:</h2>
					<DateForm className="flex-nowrap" />
				</div>
			</Container>
		</div>
		<Container>
			{!error ? <List /> : <div className="h5 text-center">{error}</div>}
		</Container>
	</>
}

export default connect(({ questions, fromdate }) => ({ questions, fromdate }))(Page);