import * as redux from 'redux';

export const KEYS = {
  QUESTIONS_PATCH: 'QUESTIONS_PATCH',
  DATE_PATCH: 'DATE_PATCH',
};

export default function store() {
  const reducers = {
    questions(
      state = [],
      action,
    ) {
      if (action.type !== KEYS.QUESTIONS_PATCH) return state;

      return action.patch;
    },

    fromdate(
      state = new Date(2018, 0 , 1).getTime(),
      action,
    ) {
      if (action.type !== KEYS.DATE_PATCH) return state;

      return action.patch;
    },

  };

  return redux.createStore(redux.combineReducers(reducers));
}