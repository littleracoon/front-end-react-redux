import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Form, Button } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { KEYS } from '../store';

function DateForm({ fromdate, className, ...props }) {

    const [startDate, setStartDate] = useState(new Date(fromdate));
    const [isChanged, setIsChanged] = useState(false);

    const handleSubmit = (event) => {
        event.preventDefault();

        const formatedValue = new Date(startDate).getTime();

        props.dispatch({
            type: KEYS.DATE_PATCH, patch: formatedValue
        });
        setIsChanged(false);
    };

    const handleChange = (date) => {
        setStartDate(date);
        setIsChanged(true);
    };

    return <Form inline className={className} onSubmit={handleSubmit}>
        <Form.Control
            as={DatePicker}
            selected={startDate}
            onChange={handleChange}
            placeholder="Search"
            className="mr-sm-2"
        />
        {isChanged && <Button variant="outline-success" type="submit">Поиск</Button>}
    </Form>
};

export default connect(({ fromdate }) => ({ fromdate }))(DateForm);